import { configureStore } from '@reduxjs/toolkit'
import employeesReducer from './reducers/employees'
import searchReducer from './reducers/search'

export default configureStore({
  reducer: {
    employees: employeesReducer,
    search: searchReducer,
  }
})