import React, { useEffect, useState } from "react";
import { setEmployees } from './reducers/employees';
import { useDispatch } from 'react-redux';
import './assets/main.scss';
import classes from './App.module.scss';

// Services
import membersService from "./services/members.service";

// Components
import BaseTable from "./components/Table/BaseTable";

function App(){
    const dispatch = useDispatch();
    const [data, setData] = useState([]);

    useEffect(  () => {
        membersService.list()
            .then( res => {
                setData(res.data);
                dispatch(setEmployees(res.data));
            })
    }, []);

    const columns = [
        { name: 'ID', text: 'ID'},
        { name: 'member_name', text: 'Ad-soyad'},
        { name: 'member_gender', text: 'Cins'},
        { name: 'member_age', text: 'Yaş'},
    ]

    return (
        <>
            <div className={classes.app}>
                <div className={classes.wrapper}>
                    <BaseTable data={data} columns={columns}/>
                </div>  
            </div>
        </>

    );
}

export default App;
