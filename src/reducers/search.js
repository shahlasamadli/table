import { createSlice } from '@reduxjs/toolkit'

export const search = createSlice({
    name: 'search',
    initialState: {
        term: ''
    },
    reducers: {
        setTerm: (state, action) => {
            state.term = action.payload
        }
    }
})

export const { setTerm } = search.actions

export default search.reducer