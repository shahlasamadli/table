import AppService from '../services/_app.service';

export default {
    list: () => {
        return AppService.get('/members');
    },

    selectById: (id) => {
        return AppService.get(`/members/${id}`);
    },

    selectBy: (params) => {
        return AppService.get(`/members?_page=${params.page}&_limit=${params.limit}&_order=${params.order}`)
    }
}