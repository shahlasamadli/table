import React from 'react';
import styles from './BaseInput.module.scss';
import { FaSearch } from "react-icons/fa";
import { setTerm } from "../../reducers/search"
import { useDispatch } from "react-redux";

const BaseInput = ({className, ...inputProps}) => {

    const dispatch = useDispatch();

    const handleInputClick = (e) => {
        dispatch(setTerm(e.target.value))
    }

    return (
        <>
            <label className={`${styles.search} ${className}`}>
                <input className={styles.search__input} {...inputProps} onInput={handleInputClick}/>
                <button className={styles.search__button}><FaSearch className={styles.search__icon}/></button>
            </label>
        </>
    );
}

export default BaseInput;