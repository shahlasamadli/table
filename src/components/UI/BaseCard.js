import React from 'react';
import styles from './BaseCard.module.scss';

const BaseCard = ({ children, className, baseCardProps }) => {

    return (
        <>
            <div className={`${className} ${styles.card}`} {...baseCardProps}>
                {children}
            </div>
        </>
    );
}

export default BaseCard;