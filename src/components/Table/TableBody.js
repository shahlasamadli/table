import React from "react";
import styles from './TableBody.module.scss';

const TableBody = (props) => {

    const data = props.data.map( d => {
        return(
            <tr key={d.id}>
                <td>{d.id}</td>
                <td>{d.member_name}</td>
                <td>{d.member_gender}</td>
                <td>{d.member_age}</td>
            </tr>
        )
    })

    return (
        <>
            {props.data.length ? <tbody className={styles.tableBody}>{data}</tbody> : <tbody className={styles.tableBody}><tr className="text-center"><td colSpan="4">No data found</td></tr></tbody>}
        </>
    )
}

export default TableBody;