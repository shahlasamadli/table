import React, { useRef } from 'react';
import styles from './Pagination.module.scss';
import classNames from "classnames";
import { $array } from "alga-js";

const Pagination = ({ total, curr, data, event, paginate }) => {
    let items = $array.pagination(total, curr, 0);

    let disablePrev = classNames({
        'page-item': true,
        'disabled': curr === 1,
    })

    let disableNext = classNames({
        'page-item': true,
        'disabled': curr === total,
    })

    return (
        <>
            { total > 1 ?
                <div className={styles.pagination}>
                    <nav aria-label="Page navigation example">
                        <ul className="pagination">
                            <li className={disablePrev}>
                                <a className="page-link" href="#" aria-label="Previous" onClick={() => event(curr - 1)}>
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>

                            {items.map(item => {
                                let points = classNames({
                                    'page-item': true,
                                    'ellipsis': item === '...',
                                    'active':  item === curr,
                                })

                                return(
                                    <li className={points} key={item}>
                                        <a className="page-link" href="#" onClick={() =>  event(item) }>{item}</a>
                                    </li>
                                )
                            })}


                            <li className={disableNext}>
                                <a className="page-link" href="#" aria-label="Next" onClick={() => event(curr + 1)}>
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                : '' }
        </>
    );
}

export default Pagination;