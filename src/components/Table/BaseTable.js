import React, { useState, useEffect } from 'react';
import styles from './BaseTable.module.scss';
import { $array, $object } from "alga-js";
import { useSelector } from "react-redux";

// Bootstrap components
import { Table } from 'react-bootstrap';

// Components
import BaseInput from '../UI/BaseInput';
import BaseCard from '../UI/BaseCard';
import TableHeader from "./TableHeader";
import TableBody from "./TableBody";
import Pagination from './Pagination';

const BaseTable = ({data, columns}) => {

    const [entries, setEntries] = useState([]);
    const [currPage, setCurrPage] = useState(1);
    const [all, setAll] = useState(1);
    const [currentEntries, setCurrentEntries] = useState(6);
    const [filteredEntries, setFilteredEntries] = useState([]);

    let searchTerm = useSelector(state => state.search.term);

    useEffect(() => {
        setEntries(data);
        paginateData(data);
    }, [data]);

    useEffect(() => {
        searchEvent();
    }, [searchTerm]);

    useEffect( () => {
        paginateEntries();
    }, [currPage])

    const paginateData = (arr) => {
        setFilteredEntries($array.paginate(arr, currPage, currentEntries));
        setAll($array.pages(arr, currentEntries));
    }

    const paginateEntries = () => {
        if(searchTerm) {
            let searchEntries = $array.search(data, searchTerm)
            paginateData(searchEntries)
        } else {
            paginateData(data);
        }
    }

    const searchEvent = () => {
        setCurrPage(1);
        paginateEntries()
    }

    return (
        <>
            <BaseInput className="mb-4" type="text" placeholder="search"/>
            <BaseCard>
                <Table striped borderless hover>
                    <TableHeader columns={columns}/>
                    <TableBody data={filteredEntries}/>
                </Table>
                <Pagination event={setCurrPage} data={filteredEntries} total={all} curr={currPage}/>
            </BaseCard>
        </>
    );
}

export default BaseTable;