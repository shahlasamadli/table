import React from "react";
import styles from './TableHeader.module.scss';

const TableHeader = (props) => {

    const columns = props.columns.map(col =>
        <th key={col.text}>{col.text}</th>
    )

    return(

        <>
            <thead className={styles.tableHead}>
                <tr>{columns}</tr>
            </thead>
        </>
    )
}

export default TableHeader;